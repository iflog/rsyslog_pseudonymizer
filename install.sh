#!/usr/bin/env bash

set -e
set -x

VENV_PATH=/usr/local/lib/rsyslog_pseudonymizer/venv
EXECUTABLE_PATH=/usr/local/lib/rsyslog_pseudonymizer/rsyslog_pseudonymizer

mkdir -p "$(dirname "$VENV_PATH")"
virtualenv -p "$(which python3)" "$VENV_PATH"
source "$VENV_PATH/bin/activate"
pip install rsyslog_pseudonymizer
deactivate

# As there are bugs within Rsyslog’s parsing of the parameter "binary"
# in version 8.32.0 (present in Ubuntu 18.04 LTS),
# we need a separate executable to not pass any parameters.

mkdir -p "$(dirname "$EXECUTABLE_PATH")"

cat << EOF > "$EXECUTABLE_PATH"
#!/usr/bin/env bash

"$VENV_PATH/bin/python3" -m rsyslog_pseudonymizer.rsyslog_pseudonymizer
EOF

chmod u+x "$EXECUTABLE_PATH"

# Configuration file for Rsyslog

cat << EOF > /etc/rsyslog.d/10-pseudonymize.conf
module(load="mmexternal")

*.* action(type="mmexternal" interface.input="fulljson" binary="$EXECUTABLE_PATH")
EOF

service rsyslog restart
