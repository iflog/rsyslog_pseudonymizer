# RsyslogPseudonymizer

RsyslogPseudonymizer is a plug-in module for Rsyslog based on Python
which replaces personal identifiers in log messages by pseudonyms
before they are written to log files.
The pseudonym-mapping is only kept in RAM.

## Test and build

Run the following commands:

```shell
virtualenv -p "$(which python3)" ./venv
source ./venv/bin/activate
pip install -r dev_requirements.txt
tox
hatch clean
hatch build
```

## Installation

In the absence of OS packages, some more steps are required.
See the provided `install.sh` or run it as root (tested on Debian).

You should then configure your applications/services
so that the logs are written only to Rsyslog.

If you use Systemd, you could configure it to forward received logs to Rsyslog.
Then it would in general only be necessary to configure applications to
only log to stdout/stderr so that Systemd handles the output.
Configuration example:

`/etc/systemd/journald.conf.d/forward_to_syslog.conf`

```
[Journal]
# Keep log data only in memory, but forward journal log to (r)syslog
Storage=volatile
ForwardToSyslog=yes

# Reduce amount of logs stored in RAM somehow
MaxFileSec=1h
MaxRetentionSec=2days
```

Then: `systemctl restart systemd-journald.service`

## License

RsyslogPseudonymizer is distributed under the terms of [Apache License, Version 2.0](https://choosealicense.com/licenses/apache-2.0).
