from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData
from rsyslog_pseudonymizer.pseudonymizer.IncrementingPseudonymDataGenerator \
    import IncrementingPseudonymDataGenerator

def test_IncrementingPseudonymDataGenerator():
    cases = [
        ("annegret", PseudonymData(0, 0)),
        ("bert", PseudonymData(1, 1)),
        ("annegret", PseudonymData(0, 0))
    ]
    pseudonymDataGenerator = IncrementingPseudonymDataGenerator()
    for (name, expected_output) in cases:

        actual_output = pseudonymDataGenerator.get_pseudonym_data(name)

        assert actual_output == expected_output
