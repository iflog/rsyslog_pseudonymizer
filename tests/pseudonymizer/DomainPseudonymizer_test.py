from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult import (
    PseudonymizeResult
)
from rsyslog_pseudonymizer.pseudonymizer.DomainPseudonymizer import (
    DomainDisassembler,
    DomainReassembler,
    DomainPseudonymizer
)
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData


# # b.d. pseudonymize domains
def test_pseudonymize_domains():
    cases = [
        ("ab.de", PseudonymizeResult("dom0.dom0", (0, 0))),
        ("de.de", PseudonymizeResult("dom1.dom0", (0, 1))),
        ("ork", None),
        ("ab.co.uk", PseudonymizeResult("dom0.dom0.dom1", (1, 0, 0)))
    ]
    domain_pseudonymizer = DomainPseudonymizer()
    for (domain, expected_output) in cases:

        actual_output = domain_pseudonymizer.get_pseudonym(domain)

        assert actual_output == expected_output, domain


# b.d.a. find domain
# b.d.b. disassemble domain
# b.d.c. reassemble domain


# b.d.a. find domain
def test_find_domain():
    cases = [
        ("ab@def.de has", "def.de"),
        ("  12-a-bc.de  ", "12-a-bc.de"),
        ("  _abc._de  ", "_abc._de"),
        ("  -abc.de  ", None),
        ("  abc.de-  ", None),
        ("  12-a-bc  ", None),
        ("  bla.co.uk  ", "bla.co.uk"),
        (" bla.de. ", "bla.de"),
        (" .bla.de ", "bla.de"),
        (" .bla.de. ", "bla.de"),
    ]
    disassembler = DomainDisassembler()
    sut = disassembler.get_regex()
    for (task, expected) in cases:

        actual = sut.search(task)

        assert (
            (expected is None and actual is None) or
            (actual is not None and expected == actual.group())
        ), task


# # b.d.b. disassemble domain
def test_disassemble_domain():
    cases = [
        ("ab.de", ("de", "ab")),
        ("de.de", ("de", "de")),
        ("xyz.com", ("com", "xyz")),
        ("ab.co.uk", ("uk", "co", "ab")),
        ("bla.de.idontexist", None),
    ]
    disassembler = DomainDisassembler()
    for (task, expected) in cases:

        actual_output = disassembler.disassemble(task)

        assert (
            (actual_output is None and expected is None) or
            (
                actual_output.data == expected and
                isinstance(actual_output.reassembler, DomainReassembler)
            )
        ), task


# # b.d.c. reassemble domain
def test_reassemble_domain():
    cases = [
        ((1, 4, 3), "dom3.dom4.dom1"),
        ((7,), "dom7")
    ]
    domain_reassembler = DomainReassembler()
    for (task, expected) in cases:

        actual = domain_reassembler.reassemble(task)

        assert actual == expected, str(task)
