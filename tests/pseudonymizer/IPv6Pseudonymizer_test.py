import pytest

from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.HierarchicalPseudonymDataGenerator \
    import HierarchicalPseudonymDataGenerator
from rsyslog_pseudonymizer.pseudonymizer.IPv6Pseudonymizer import (
    IPv6Disassembler,
    IPv6Reassembler,
    IPv6Pseudonymizer
)
from rsyslog_pseudonymizer.pseudonymizer.DisassembleResult \
    import DisassembleResult
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData \
    import PseudonymData
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult \
    import PseudonymizeResult


# # b.b. pseudonymize IPv6s
def test_pseudonymize_ipv6():
    cases = [
        (
            "2001:DB8:0:0:8:800:200C:417A",
            PseudonymizeResult("::", (0, 0, 0, 0, 0, 0, 0, 0))
        ),
        (
            "2001:db8:0:0:9:800:200c:417a",
            PseudonymizeResult("::1:0:0:0", (0, 0, 0, 0, 1, 0, 0, 0))
        )
    ]
    pseudonymizer = IPv6Pseudonymizer()
    for (task, expected) in cases:

        found_ip = pseudonymizer \
            .get_regex() \
            .search("Bla " + task + "...") \
            .group()
        actual_output = pseudonymizer.get_pseudonym(found_ip)

        assert actual_output == expected, str(task)

# b.b.a. find IPv6s
# b.b.b. get data structure from IPv6 (disassemble)
# b.b.c. pseudonymize IPv6 data structure
# b.b.d. reassemble IPv6 from data structure


# # b.b.a. find IPv6s
def test_find_ipv6():
    cases = [
        (
            "  <ABCD:EF01:2345:6789:ABCD:EF01:2345:6789> ",
            "ABCD:EF01:2345:6789:ABCD:EF01:2345:6789"
        ),
        ("  2001:DB8:0:0:8:800:200C:417A ", "2001:DB8:0:0:8:800:200C:417A"),
        ("2001:DB8:0:8:800:200C:417A", None),
        ("2001:DB8:0:0:4:8:800:200C:417A", "2001:DB8:0:0:4:8:800:200C"),
        ("2001:DB8::8:800:200C:417A", "2001:DB8::8:800:200C:417A"),
        ("FF01::101", "FF01::101"),
        ("::1", "::1"),
        (" 9:: ", "9::"),
        (" 9:12:: ", "9:12::"),
        ("::", "::"),
        ("", None),
        ("::1::", "::1"),
        ("1:2:3::1.2.4.8", "1:2:3::1.2.4.8"),
        ("::1:2:3:1.2.4.8", "::1:2:3:1.2.4.8"),
        ("::1:2:3:1.2.4", "::1:2:3:1"),
        ("::1:2::1.2.4.8", "::1:2"),
        ("2019-03-30T00:40:27+01:00", None),
    ]

    sut = IPv6Disassembler().get_regex()
    for (text, match_expected) in cases:

        match = sut.search(text)

        assert (
            (match_expected is None and match is None) or (
                match_expected is not None and match is not None and
                match_expected == match.group()
            )
        ), text

def test_find_all_ipv6():
    cases = [
        (
            "2019-03-30T00:40:27+01:00 2a02:8071:2ca9:fc00::3",
            "2a02:8071:2ca9:fc00::3"
        ),
    ]

    sut = IPv6Disassembler().get_regex()
    for (text, match_expected) in cases:

        matches = sut.findall(text)

        assert match_expected in matches, text


# # b.b.b. get data structure from IPv6 (disassemble)
def test_disassemble_ipv6():
    cases = [
        (
            "ABCD:EF01:2345:6789:ABCD:EF01:2345:6789",
            DisassembleResult(
                (43981, 61185, 9029, 26505, 43981, 61185, 9029, 26505),
                IPv6Reassembler(False, False)
            )
        ),
        (
            "2001:db8:0:0:8:800:200c:417a",
            DisassembleResult(
                (8193, 3512, 0, 0, 8, 2048, 8204, 16762),
                IPv6Reassembler(True, False)
            )
        ),
        (
            "2001:DB8::8:800:200C:417A",
            DisassembleResult(
                (8193, 3512, 0, 0, 8, 2048, 8204, 16762),
                IPv6Reassembler(False, False)
            )
        ),
        (
            "ff01::101",
            DisassembleResult(
                (65281, 0, 0, 0, 0, 0, 0, 257),
                IPv6Reassembler(True, False)
            )
        ),
        (
            "::1",
            DisassembleResult(
                (0, 0, 0, 0, 0, 0, 0, 1),
                IPv6Reassembler(False, False)
            )
        ),
        (
            "::",
            DisassembleResult(
                (0, 0, 0, 0, 0, 0, 0, 0),
                IPv6Reassembler(False, False)
            )
        ),
        (
            "1:2:3::1.2.4.8",
            DisassembleResult(
                (1, 2, 3, 0, 0, 0, 258, 1032),
                IPv6Reassembler(False, True)
            )
        ),
        (
            "::a:2:3:1.2.4.8",
            DisassembleResult(
                (0, 0, 0, 10, 2, 3, 258, 1032),
                IPv6Reassembler(True, True)
            )
        ),
        ("1:2:3:4:5:6:7", None),
        ("1:2:3:4:5:6:7:8:9", None),
    ]

    sut = IPv6Disassembler()
    for (ipv6, expected_data) in cases:

        actual_result = sut.disassemble(ipv6)

        assert actual_result == expected_data, str(ipv6)


# # b.b.c. pseudonymize IPv6 data structure
# see HierarchicalPseudonymDataGenerator


# # b.b.d. reassemble IPv6 from data structure
def test_reassemble_ipv6():
    cases = [
        (
            (43981, 61185, 9029, 26505, 43981, 61185, 9029, 26505),
            False, False,
            'ABCD:EF01:2345:6789:ABCD:EF01:2345:6789'
        ),
        (
            (43981, 61185, 9029, 26505, 43981, 61185, 9029, 26505),
            True, False,
            'abcd:ef01:2345:6789:abcd:ef01:2345:6789'
        ),
        (
            (8193, 3512, 0, 0, 8, 2048, 8204, 16762),
            False, False,
            '2001:DB8::8:800:200C:417A'
        ),
        ((65281, 0, 0, 0, 0, 0, 0, 257), False, False, 'FF01::101'),
        ((0, 0, 0, 0, 0, 0, 0, 1), True, False, '::1'),
        ((0, 0, 0, 0, 0, 0, 0, 0), False, False, '::'),
        ((1, 2, 3, 0, 0, 0, 258, 1032), True, True, '1:2:3::1.2.4.8'),
        ((0, 0, 0, 1, 2, 3, 258, 1032), False, True, '::1:2:3:1.2.4.8')
    ]
    for (pseudonymized_data, lowercase, ipv4_representation, expected_output) \
            in cases:
        reassembler = IPv6Reassembler(lowercase, ipv4_representation)

        actual_output = reassembler.reassemble(pseudonymized_data)

        assert actual_output == expected_output, str(pseudonymized_data)
