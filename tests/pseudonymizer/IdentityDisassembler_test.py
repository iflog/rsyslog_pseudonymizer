from rsyslog_pseudonymizer.pseudonymizer.IdentityDisassembler import (
    IdentityDisassembler
)
from rsyslog_pseudonymizer.pseudonymizer.DisassembleResult import (
    DisassembleResult
)
from rsyslog_pseudonymizer.pseudonymizer.Reassembler import Reassembler

def test_IdentityDisassembler():
    class FakeReassembler(Reassembler):
        def reassemble(self, flat_data):
            if flat_data == 4:
                return "dummy"
            raise ValueError()

    reassembler = FakeReassembler()
    sut = IdentityDisassembler(reassembler)
    expected = DisassembleResult("test", reassembler)

    result = sut.disassemble("test")

    assert result == expected
