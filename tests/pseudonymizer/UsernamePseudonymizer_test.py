from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult import (
    PseudonymizeResult
)
from rsyslog_pseudonymizer.pseudonymizer.UsernamePseudonymizer import (
    UsernamePseudonymizer,
    UsernameReassembler
)

# # b.c. pseudonymize user names
def test_pseudonymize_user_names():
    cases = [
        ("heinz", PseudonymizeResult("user0", 0)),
        ("greta", PseudonymizeResult("user1", 1))
    ]
    pseudonymizer = UsernamePseudonymizer()
    for (username, expected_output) in cases:

        actual_output = pseudonymizer.get_pseudonym(username)

        assert actual_output == expected_output

def test_UsernameReassembler():
    cases = [
        (0, "user0"),
        (14, "user14")
    ]
    sut = UsernameReassembler()
    for (data, name) in cases:

        actual = sut.reassemble(data)

        assert actual == name
