import re

from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult import (
    PseudonymizeResult
)
from rsyslog_pseudonymizer.pseudonymizer.EmailPseudonymizer import (
    EmailDisassembler,
    EmailReassembler,
    EmailPseudonymizer
)
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData

# # b.e. pseudonymize emails
class FakeUsernamePseudonymizer(Pseudonymizer):
    def __init__(self):
        super().__init__(None, None)

    def get_pseudonym(self, name):
        if name == "ab":
            return PseudonymizeResult(formatted="user0", deep_data=0)
        raise ValueError()


class FakeDomainPseudonymizer(Pseudonymizer):
    def __init__(self):
        super().__init__(None, None)

    def get_regex(self):
        return re.compile(r"\w+\.\w+")

    def get_pseudonym(self, name):
        if name == "def.x":
            return PseudonymizeResult(formatted="dom1.dom0", deep_data=(0, 1))
        raise ValueError()


def test_pseudonymize_emails():
    email = "ab@def.x"
    domain_pseudonymizer = FakeDomainPseudonymizer()
    username_pseudonymizer_factory = FakeUsernamePseudonymizer
    email_pseudonymizer = EmailPseudonymizer(
        domain_pseudonymizer,
        username_pseudonymizer_factory
    )
    expected_output = PseudonymizeResult(
        formatted="user0@dom1.dom0",
        deep_data=((0, 1), 0)
    )

    actual_output = email_pseudonymizer.get_pseudonym(email)

    assert actual_output == expected_output

# b.e.a. find email
# b.e.b. disassemble email
# b.e.c. generate email pseudonym data
# b.e.d. reassemble email


# # b.e.a. find email
def test_find_email():
    cases = [
        (" a.b@def ", "a.b@def"),
        (" 1@xyz.nt;", "1@xyz.nt"),
        ("  .@ab", None),
        ("", None),
        ("a@.e.b", None),
        ("@a", None),
        ("a.b", None),
        ("a_b+13c@t31.d98", "a_b+13c@t31.d98")
    ]
    sut = EmailDisassembler(r"\w+\.\w+").get_regex()
    for (email, expected) in cases:

        match = sut.search(email)

        assert (
            (expected is None and match is None) or
            (match is not None and expected == match.group())
        ), email


# # b.e.b. disassemble email
def test_disassemble_email():
    email = "ab@def.x"
    sut = EmailDisassembler(r"\w\.\w")
    expected_data = ("def.x", "ab")

    actual_result = sut.disassemble(email)

    assert actual_result.data == expected_data
    assert isinstance(actual_result.reassembler, EmailReassembler)


# # b.e.c. generate email pseudonym data
# See UsernameDomainPseudonymDataGenenator_test

# # b.e.d. reassemble email
def test_reassemble_email():
    data = ("dom1", "user0")
    expected_output = "user0@dom1"
    reassembler = EmailReassembler()

    actual_output = reassembler.reassemble(data)

    assert actual_output == expected_output
