from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult import (
    PseudonymizeResult
)
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData
from rsyslog_pseudonymizer.pseudonymizer.UsernameDomainPseudonymDataGenerator \
    import UsernameDomainPseudonymDataGenerator

class FakeUsernamePseudonymizer(Pseudonymizer):
    def __init__(self):
        super().__init__(None, None)

    def get_pseudonym(self, name):
        if name == "ab":
            return PseudonymizeResult(formatted="user0", deep_data=0)
        raise ValueError()


class FakeDomainPseudonymizer(Pseudonymizer):
    def __init__(self):
        super().__init__(None, None)

    def get_pseudonym(self, name):
        if name == "def.x":
            return PseudonymizeResult(formatted="dom1.dom0", deep_data=(0, 1))
        raise ValueError()

def test_generate_pseudonym_data():
    name_data = ("def.x", "ab")
    expected = PseudonymData(flat=("dom1.dom0", "user0"), deep=((0, 1), 0))
    domain_pseudonymizer = FakeDomainPseudonymizer()
    username_pseudonymizer_factory = FakeUsernamePseudonymizer
    pseudonym_data_generator = UsernameDomainPseudonymDataGenerator(
        domain_pseudonymizer,
        username_pseudonymizer_factory
    )

    actual_result = pseudonym_data_generator.get_pseudonym_data(name_data)

    assert actual_result == expected
