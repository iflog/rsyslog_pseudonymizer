import re

from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult \
    import PseudonymizeResult
from rsyslog_pseudonymizer.pseudonymizer.MatrixIDPseudonymizer import (
    MatrixIDDisassembler,
    MatrixIDReassembler,
    MatrixIDPseudonymizer
)
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData

# # b.e. pseudonymize MatrixIDs
class FakeUsernamePseudonymizer(Pseudonymizer):
    def __init__(self):
        super().__init__(None, None)

    def get_pseudonym(self, name):
        if name == "ab":
            return PseudonymizeResult(formatted="user0", deep_data=0)
        raise ValueError()


class FakeDomainPseudonymizer(Pseudonymizer):
    def __init__(self):
        super().__init__(None, None)

    def get_regex(self):
        return re.compile(r"\w+\.\w+")

    def get_pseudonym(self, name):
        if name == "def.x":
            return PseudonymizeResult(formatted="dom1.dom0", deep_data=(0, 1))
        raise ValueError()


def test_pseudonymize_matrix_ids():
    matrix_id = "@ab:def.x"
    domain_pseudonymizer = FakeDomainPseudonymizer()
    username_pseudonymizer_factory = FakeUsernamePseudonymizer
    matrix_id_pseudonymizer = MatrixIDPseudonymizer(
        domain_pseudonymizer,
        username_pseudonymizer_factory
    )
    expected_output = PseudonymizeResult(
        formatted="@user0:dom1.dom0",
        deep_data=((0, 1), 0)
    )

    actual_output = matrix_id_pseudonymizer.get_pseudonym(matrix_id)

    assert actual_output == expected_output

# b.e.a. find Matrix ID
# b.e.b. disassemble Matrix ID
# b.e.c. generate Matrix ID pseudonym data
# b.e.d. reassemble Matrix ID


# # b.e.a. find Matrix ID
def test_find_matrix_id():
    cases = [
        (" @a.b:def ", "@a.b:def"),
        (" @1:xyz.nt;", "@1:xyz.nt"),
        ("  @.:ab", None),
        ("", None),
        ("@a:.e.b", None),
        ("@:eb.de", None),
        ("@ab:", None),
        ("@a_b+13c:t31.d98", "@a_b+13c:t31.d98")
    ]
    sut = MatrixIDDisassembler(r"\w+\.\w+").get_regex()
    for (matrix_id, expected) in cases:

        match = sut.search(matrix_id)

        assert (
            (expected is None and match is None) or
            (match is not None and expected == match.group())
        ), matrix_id


# # b.e.b. disassemble Matrix ID
def test_disassemble_matrix_id():
    matrix_id = "@ab:def.x"
    sut = MatrixIDDisassembler(r"\w\.\w")
    expected_data = ("def.x", "ab")

    actual_result = sut.disassemble(matrix_id)

    assert actual_result.data == expected_data
    assert isinstance(actual_result.reassembler, MatrixIDReassembler)


# # b.e.c. generate Matrix ID pseudonym data
# See UsernameDomainPseudonymDataGenenator_test


# # b.e.d. reassemble Matrix ID
def test_reassemble_matrix_id():
    data = ("dom1", "user0")
    expected_output = "@user0:dom1"
    reassembler = MatrixIDReassembler()

    actual_output = reassembler.reassemble(data)

    assert actual_output == expected_output
