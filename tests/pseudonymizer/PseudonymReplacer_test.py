import re
import sys

from rsyslog_pseudonymizer.pseudonymizer.PseudonymReplacer import (
    PseudonymReplacer,
    get_default_identifier_types,
    _pseudonymize_text_with_identifiers
)
from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult import (
    PseudonymizeResult
)

'''
- <X>Disassembler
    - `regex`: matching what be could
      probably be parsed (for efficiency)
    - `disassemble: (self, name: string) ->
      (data: <X>DisassembledData, reassembler: <X>Reassembler) | None`
        - converts `name` into a disassembled form
        - `data` contains the "data" of the identity
        - `reassembler` contains information,
          how the identity string can be restored from `data`
        - if a semantically equal entity is represented
          in different ways as a string,
          `data` should be equal but `reassembler` should be the same
        - The `reassembler` converts "data" back to the original representation
        - If the original `data` are given to `reassembler`,
          the returned string should be very similar to the original string
          (as long as privacy can be achieved)
- <X>PseudonymDataGenerator:
    - `get_pseudonym_data: (self, data: <X>DisassembledData) ->
      (flat: <X>FlatData, deep: <X>DeepData)`
    - Takes the `data` of a parsed string to pseudonymize and
      returns a data structure representing the pseudonym
    - May return the same pseudonym `data` for different original `data`
      when it is necessary for privacy
- <X>Reassembler
    - `reassemble: (self, data: <X>FlatData) -> string
    - restores the formatted identity string from the data
    - class does not need to be exposed
- Pseudonymizer:
    - `get_pseudonym: (self, name: string) ->
      (raw: <X>DeepData, formatted: string)
    - generates a pseudonym string and the according pseudonymized unified data
      from a string
    - can be created from a combination of
      an <X>Disassembler and an <X>PseudonymDataGenerator
    - is the only class being needed to be exposed if the <X>
      pseudonymization shall be reusable in compositions,
      however it is more flexible when the other classes are exposed, too
'''

# Top-down design

# # The whole process
def test_pseudonymize_text():
    test_input = "User ab@def.de has IP 19.245.114.48 and " \
        "user cd@def.de has IP 19.245.114.51"
    sut = PseudonymReplacer(get_default_identifier_types())
    output = sut.pseudonymize_text(test_input)
    assert output == "User user0@dom0.dom0 has IP 0.0.0.0 and " \
        "user user1@dom0.dom0 has IP 0.0.0.1"

def test_domain_name_order():
    test_input = "else.us other.de another.de " + \
        "to=<bob@abc.de>, relay=mail01.abc.de[9::]:25"
    sut = PseudonymReplacer(get_default_identifier_types())
    output = sut.pseudonymize_text(test_input)
    output_pattern = re.compile(
        r"^dom(?P<us_else>\d+)\.dom(?P<us>\d+)" + \
        r" dom(?P<de_other>\d+)\.dom(?P<de>\d+)" + \
        r" dom(?P<de_another>\d+)\.dom(?P<de1>\d+)" + \
        r" to=<user0@dom(?P<de_abc>\d+)\.dom(?P<de2>\d+)>," + \
        r" relay=dom(?P<de_abc_mail01>\d+)" + \
        r"\.dom(?P<de_abc1>\d+)\.dom(?P<de3>\d+)\[::\]:25$")
    match = output_pattern.search(output)
    assert match is not None
    exact_values = [
        ("us_else", "0"),
        ("de_abc_mail01", "0"),
    ]
    equivalences = [
        {"de", "de1", "de2", "de3"},
        {"de_abc", "de_abc1"}
    ]
    inequivalences = [
        {"us", "de"},
        {"de_other", "de_another", "de_abc"}
    ]
    for (group_name, value) in exact_values:
        assert match.group(group_name) == value
    for equivalence in equivalences:
        assert len(set(map(match.group, equivalence))) == 1
    for inequivalence in inequivalences:
        length = len(inequivalence)
        assert len(set(map(match.group, inequivalence))) == length

def test_domain_names_with_number():
    test_input = "relay=mail01.abc.de[9::]:25"
    sut = PseudonymReplacer(get_default_identifier_types())
    output = sut.pseudonymize_text(test_input)
    assert "relay=dom0.dom0.dom0[::]:25" == output


def test_regex_false_positive_before_regex_true_positive():
    test_input = '2019-03-30T00:40:27+01:00 2a02:8071:2ca9:fc00::3'
    sut = PseudonymReplacer(get_default_identifier_types())
    output = sut.pseudonymize_text(test_input)
    assert '2a02:8071:2ca9:fc00::3' not in output


# a. split the identifiers apart
# b. pseudonymize the individual identifiers
# c. join the text again


# # a. split the identifiers apart
def test_pseudonymize_text_with_identifiers():
    class FakeEmailPseudonymizer(Pseudonymizer):
        def __init__(self):
            self.__regex = re.compile(r'\w+@\w+\.\w+')

        def get_pseudonym(self, text):
            if text == "ab@def.x":
                return PseudonymizeResult("user0@dom0.dom0", ())
            raise ValueError(text)

        def get_regex(self):
            return self.__regex

    class FakeDomainPseudonymizer(Pseudonymizer):
        def __init__(self):
            self.__regex = re.compile(r'\w+\.\w+')

        def get_pseudonym(self, text):
            if text == "def.x":
                return PseudonymizeResult("dom0.dom0", ())
            if text == "def.de":
                return PseudonymizeResult("dom0.dom1", ())
            raise ValueError(text)

        def get_regex(self):
            return self.__regex

    email_pseudonomizer = FakeEmailPseudonymizer()
    domain_pseudonomizer = FakeDomainPseudonymizer()
    identifier_types = [
        domain_pseudonomizer,
        email_pseudonomizer
    ]
    text = "User ab@def.x from def.de"
    expected_output = "User user0@dom0.dom0 from dom0.dom1"

    actual_output = _pseudonymize_text_with_identifiers(identifier_types, text)

    assert actual_output == expected_output

# b.a. pseudonymize IPv4s
# b.b. pseudonymize IPv6s
# b.c. pseudonymize user names
# b.d. pseudonymize domains
# b.e. pseudonymize email adresses
