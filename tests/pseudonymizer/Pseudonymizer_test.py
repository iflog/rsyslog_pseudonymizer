import re

from rsyslog_pseudonymizer.pseudonymizer.Pseudonymizer import Pseudonymizer
from rsyslog_pseudonymizer.pseudonymizer.Reassembler import Reassembler
from rsyslog_pseudonymizer.pseudonymizer.Disassembler import Disassembler
from rsyslog_pseudonymizer.pseudonymizer.PseudonymDataGenerator \
    import PseudonymDataGenerator
from rsyslog_pseudonymizer.pseudonymizer.DisassembleResult \
    import DisassembleResult
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult \
    import PseudonymizeResult

def test_Pseudonymizer():
    cases = [
        ("456", PseudonymizeResult("123", (1, 2, 3))),
        ("45", None),
        ("4567", None)
    ]
    class FakeReassembler(Reassembler):
        def reassemble(self, flat_data):
            if flat_data == ("1", "2", "3"):
                return "123"
            raise ValueError(flat_data)

    class FakeDisassembler(Disassembler):
        def __init__(self):
            self.__regex = re.compile(" abc ")

        def get_regex(self):
            return self.__regex

        def disassemble(self, text):
            if text == "456":
                return DisassembleResult(("4", "5", "6"), FakeReassembler())
            if text == "45":
                return DisassembleResult(("4", "5"), FakeReassembler())
            if text == "4567":
                return None
            raise ValueError()

    class FakePseudonymDataGenerator(PseudonymDataGenerator):
        def get_pseudonym_data(self, data):
            if data == ("4", "5", "6"):
                return PseudonymData((1, 2, 3), ("1", "2", "3"))
            if data == ("4", "5"):
                return None
            raise ValueError()

    disassembler = FakeDisassembler()
    pseudonymizer = Pseudonymizer(
        disassembler,
        FakePseudonymDataGenerator()
    )

    assert pseudonymizer.get_regex() == disassembler.get_regex()

    for (task, expected) in cases:

        actual = pseudonymizer.get_pseudonym(task)

        assert actual == expected
