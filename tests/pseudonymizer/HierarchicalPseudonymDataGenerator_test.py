from rsyslog_pseudonymizer.pseudonymizer.HierarchicalPseudonymDataGenerator \
    import HierarchicalPseudonymDataGenerator
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData

def test_generate_hierarchical_pseudonym():
    sut = HierarchicalPseudonymDataGenerator()
    cases = [
        ((5,), (0,)),
        ((), ()),
        ((5, 2), (0, 0)),
        ((6, 2), (1, 0)),
        ((5, 2), (0, 0)),
        ((5, 1), (0, 1)),
        ((6, 3), (1, 1)),
    ]

    for (task, expected) in cases:
        actual_output = sut.get_pseudonym_data(task)
        assert actual_output == PseudonymData(expected, expected), str(task)
