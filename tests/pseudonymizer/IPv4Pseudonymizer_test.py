from rsyslog_pseudonymizer.pseudonymizer.HierarchicalPseudonymDataGenerator \
    import HierarchicalPseudonymDataGenerator
from rsyslog_pseudonymizer.pseudonymizer.IPv4Pseudonymizer import (
    IPv4Disassembler,
    IPv4Reassembler,
    IPv4Pseudonymizer
)
from rsyslog_pseudonymizer.pseudonymizer.PseudonymData import PseudonymData
from rsyslog_pseudonymizer.pseudonymizer.PseudonymizeResult \
    import PseudonymizeResult

# # b.a. pseudonymize IPv4s
def test_pseudonymize_ipv4():
    class FakeHierarchicalPseudonymDataGenerator(object):
        def get_pseudonym_data(self, name):
            if name == (248, 15, 0, 17):
                return PseudonymData((0, 1, 0, 2), (0, 1, 0, 2))
            raise ValueError()

    ip = "248.15.0.17"
    pseudonymizer = IPv4Pseudonymizer(
        data_generator=FakeHierarchicalPseudonymDataGenerator())
    actual_output = pseudonymizer.get_pseudonym(ip)
    expected_output = PseudonymizeResult("0.1.0.2", (0, 1, 0, 2))
    assert actual_output == expected_output

# b.a.a. find IPv4s
# b.a.b. get data structure from IPv4 (disassemble)
# b.a.c. pseudonymize IPv4 data structure
# b.a.d. reassemble IPv4 from data structure


# # b.a.a. find IPv4s
def test_find_ipv4():
    cases = [
        ("  12.12.12.12 ", "12.12.12.12"),
        ("  0.001.255.37 ", "0.001.255.37"),
        ("  0.001.256.37 ", None),
        ("  0.001.255.37 ", "0.001.255.37"),
        ("  12.12.12.12 ", "12.12.12.12"),
        ("  12.12.12.12 ", "12.12.12.12"),
    ]

    sut = IPv4Disassembler().get_regex()
    for (text, match_expected) in cases:

        match = sut.search(text)

        assert (
            (match_expected is None and match is None) or
            (match_expected == match.group())
        )


# # b.a.b. get data structure from IPv4 (disassemble)
def test_disassemble_ipv4():
    ip = "248.15.0.17"
    sut = IPv4Disassembler()
    expected_data = (248, 15, 0, 17)

    actual_result = sut.disassemble(ip)

    assert actual_result.data == expected_data
    assert isinstance(actual_result.reassembler, IPv4Reassembler)


# # b.a.c. pseudonymize IPv4 data structure
# b.a.c.a. HierarchicalPseudonymDataGenerator

# # b.a.d. reassemble IPv4 from data structure
def test_reassemble_ipv4():
    reassembler = IPv4Reassembler()
    pseudonymized_data = (0, 1, 0, 25)
    expected_output = "0.1.0.25"

    actual_output = reassembler.reassemble(pseudonymized_data)

    assert actual_output == expected_output
